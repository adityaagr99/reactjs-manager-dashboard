import {
    NEW_NOFIFICATION
} from "../types"

const initialState = {
    mess: "",
    type: ""
}

export default function(state = initialState, action) {
    switch (action.type) {
        case NEW_NOFIFICATION:
            return {
                ...state,
                mess: action.payload.mess,
                type: action.payload.type
            }
        default:
            return state
    }
}