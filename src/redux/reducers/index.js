import {
    combineReducers
} from 'redux'
import authReducer from './auth.reducer';
import orderReducer from './order.reducer';
import notificationReducer from './notification.reducer';
import categoryReducer from './category.reducer';
import errorsReducer from './errors.reducer';


export default combineReducers({
    auth: authReducer,
    order: orderReducer,
    notification: notificationReducer,
    category: categoryReducer,
    errors: errorsReducer
})