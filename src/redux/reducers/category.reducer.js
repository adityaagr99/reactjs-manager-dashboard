import {
  GET_ALL_CATEGORY,
  SET_LOADING,
  TOGGLE_FOOD_ITEM,
  SET_PENDING,
  GET_ALL_ADDON,
  TOGGLE_ADDON,
  GET_ALL_CUSTOMER_ORDER

} from "../types"

const initialState = {
  loading: false,
  pending: false,
  list: [],
  addonList: [],
  customerOrder: []
}
export default function (state = initialState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: true
      };
    case SET_PENDING:
      return {
        ...state,
        pending: true
      };

    case GET_ALL_CATEGORY:
      return {
        ...state,
        list: action.payload,
          loading: false
      };
    case TOGGLE_FOOD_ITEM:
      let toggleData = [...state.list]
      toggleData.forEach(item => {
        item.data.forEach(e => {
          if (e.foodid === action.payload) {
            e.isavail = !e.isavail
          }
        })
      })
      return {
        ...state,
        list: toggleData,
          pending: false
      };
    case GET_ALL_ADDON:
      return {
        ...state,
        addonList: action.payload
      };
    case TOGGLE_ADDON:
      let toggleAddonData = [...state.addonList]
      toggleAddonData.forEach(item => {
        if (item.addonid === action.payload) {
          item.is_avail = !item.is_avail
        }
      })
      return {
        ...state,
        addonList: toggleAddonData
      };
    case GET_ALL_CUSTOMER_ORDER:
      console.log(action.payload.data);

      let tempData = action.payload.data.filter((item, index,
          self) =>
        index === self.findIndex(t => t.foodname === item.foodname))
      return {
        ...state, customerOrder: tempData
      };
    default:
      return state;
  }
}
