import {
    GET_ERROR,
} from "../types"

const ininitalState = {
    text: ""
}
export default function(state = ininitalState, action) {
    switch (action.type) {
        case GET_ERROR:
            return {
                ...state,
                text: action.payload
            }
        default:
            return state
    }
}