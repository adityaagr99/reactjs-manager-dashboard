import {
  GET_ALL_ORDER,
  SET_LOADING,
  DELETE_FOOD_ITEM,
  ACCEPT_PAYMENT,
  GET_ORDER_BILL,
  ADD_OTHER_ITEM,
  REMOVE_CUSTOM_ITEM,
  GET_OTHER_ITEM,
  SET_TABLE_ID_TIME,
} from "../types"


const ininitalState = {
  loading: false,
  orderlist: [],
  billInfo: {},
  other_items: {
    food_items: [],
    is_placed: true,
    order_identifier: "",
    order_time: ""
  },
  tableidtimestamp: null,
  paymentState: false,
  addonList: []
}
export default function (state = ininitalState, action) {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_ALL_ORDER:
      return {
        ...state,
        orderlist: action.payload,
          loading: false,
      };
    case GET_ORDER_BILL:
      return {
        ...state,
        loading: false,
          orderlist: action.payload
      };
    case ADD_OTHER_ITEM:
      return {
        ...state,
        orderlist: state.orderlist.map(item => {
          if (item.tableidtimestamp === action.payload
            .tableidtimestamp) {
            item.orders.map(order => {
              if (order.order_identifier === action.payload
                .order_identifier) {
                return order.food_items.push(action.payload
                  .data)
              }
              return order
            })
          }
          return item
        })
      };
    case DELETE_FOOD_ITEM:
      const tempData = [...state.orderlist]
      tempData.forEach(item => {
        if (item.tableidtimestamp === action.payload
          .tableidtimestamp) {
          item.orders = action.payload.orders
        }
      });
      return {
        ...state,
        orderlist: tempData
      };
    case ACCEPT_PAYMENT:
      return {
        ...state,
        paymentState: action.payload.check,
          orderlist: state.orderlist.filter(e => e
            .tableidtimestamp !== action.payload.tableidtimestamp)
      };
    case REMOVE_CUSTOM_ITEM:
      let tempDataCustom = [...state.orderlist]
      tempDataCustom.forEach(item => {
        if (item.tableidtimestamp === action.payload
          .tableidtimestamp) {
          let newgroup = item.orders[item.orders.length - 1]
            .food_items.filter(e => e.foodid !== action.payload
              .id)
          item.orders[item.orders.length - 1]
            .food_items = newgroup
        }
      });
      return {
        ...state,
        orderlist: tempDataCustom
      };
    case SET_TABLE_ID_TIME:
      return {
        ...state,
        tableidtimestamp: action.payload,
          other_items: {
            ...state.other_items,
            food_items: action.payload !== state
              .tableidtimestamp ? [] : state.other_items
              .food_items
          }
      };
    case GET_OTHER_ITEM:
      return {
        ...state,
        other_items: {
          ...state.other_items,
          food_items: [...state.other_items
            .food_items, action
            .payload.otheritem_data
          ],
          order_identifier: action.payload
            .other_order_identifier,
          order_time: action.payload.order_time
        }
      };

    default:
      return state;
  }
}
