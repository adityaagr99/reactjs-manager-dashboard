import {
    SET_LOADING,
    SET_CURRENT_USER,
    LOGOUT,

} from "../types";

const initialState = {
    loading: false,
    isAdmin: false,
    caffeDetails: null,
    caffeID: "",
}
export default function(state = initialState, action) {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: true
            }
        case SET_CURRENT_USER:
            return {
                ...state,
                isAdmin: action.payload.isAdmin,
                caffeID: action.payload.caffeID,
                caffeDetails: action.payload.caffeDetails,
                loading: false
            }
        case LOGOUT:
            return {
                ...state,
                isAdmin: false
            }
        default:
            return state
    }
}