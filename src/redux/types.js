export const GET_ERROR = "GET_ERROR"
export const SET_LOADING = 'SET_LOADING';
export const SET_PENDING = 'SET_PENDING';

// auth
export const LOGIN = 'LOGIN';
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const LOGOUT = "LOGOUT";


// order manager
export const GET_ALL_ORDER = "GET_ALL_ORDER"
export const DELETE_FOOD_ITEM = "DELETE_FOOD_ITEM"
export const ACCEPT_PAYMENT = "ACCEPT_PAYMENT"
export const ADD_OTHER_ITEM = "ADD_OTHER_ITEM"
export const REMOVE_CUSTOM_ITEM = "REMOVE_CUSTOM_ITEM"
export const GET_OTHER_ITEM = "GET_OTHER_ITEM"
export const SET_TABLE_ID_TIME = "SET_TABLE_ID_TIME"

// notification
export const NEW_NOFIFICATION = "NEW_NOFIFICATION"

// category
export const GET_ALL_CATEGORY = "GET_ALL_CATEGORY"
export const GET_FOOD_ITEM = "GET_FOOD_ITEM"
export const DISABLE_FOOD_ITEM = "DISABLE_FOOD_ITEM"
export const ENABLE_FOOD_ITEM = "ENABLE_FOOD_ITEM"
export const TOGGLE_FOOD_ITEM = "TOGGLE_FOOD_ITEM"
export const GET_ALL_ADDON = "GET_ALL_ADDON"
export const TOGGLE_ADDON = "TOGGLE_ADDON"
export const GET_ALL_CUSTOMER_ORDER = "GET_ALL_CUSTOMER_ORDER"

// print bill
export const GET_ORDER_BILL = "GET_ORDER_BILL"
export const PRINT_BILL = "PRINT_BILL"
