import {
    NEW_NOFIFICATION
} from "../types"

export const newNotification = (mess) => dispatch => {
    if (mess.indexOf("NEW CLIENT") !== -1) {
        dispatch({
            type: NEW_NOFIFICATION,
            payload: {
                mess: `New client added at table no ${mess.slice(mess.lastIndexOf("@")+1)}`,
                type: 'NEW'
            }
        })
    }
    if (mess.indexOf("PAYMENT-Cash") !== -1 || mess.indexOf("PAYMENT-Card") !==
        -1) {
        dispatch({
            type: NEW_NOFIFICATION,
            payload: {
                mess: `Payment requested from table no ${mess.slice(mess.lastIndexOf("@")+1)}`,
                type: 'UPDATED-PAYMENT'
            }
        })
    }
    if (mess.indexOf("NEW_ORDER") !== -1) {
        dispatch({
            type: NEW_NOFIFICATION,
            payload: {
                mess: `Order added at table no ${mess.slice(mess.lastIndexOf("@")+1)}`,
                type: 'NEW'
            }
        })
    }
    if (mess.indexOf("ORDER_UPDATED") !== -1) {
        dispatch({
            type: NEW_NOFIFICATION,
            payload: {
                mess: `Order is updated at table no ${mess.slice(mess.lastIndexOf("@")+1)}`,
                type: 'UPDATED-ORDER'
            }
        })
    }
    if (mess.indexOf("ISSUE_RAISED") !== -1) {
        dispatch({
            type: NEW_NOFIFICATION,
            payload: {
                mess: `New Issue raised at Table no.${mess.slice(mess.lastIndexOf("@")+1)}`,
                type: 'ISSUE_RAISED'
            }
        })
    }

}