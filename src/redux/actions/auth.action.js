import axios from 'axios';
import {
    SET_LOADING,
    SET_CURRENT_USER,
    GET_ERROR,
    LOGOUT,

} from '../types';
const cors = "https://cors-anywhere.herokuapp.com/"
export const setLoading = () => ({
    type: SET_LOADING
})

export const setCurrentUser = (isAdmin, caffeID, caffeDetails) => ({
    type: SET_CURRENT_USER,
    payload: {
        isAdmin,
        caffeID,
        caffeDetails
    }
})

export const login = userData => dispatch => {
    dispatch(setLoading());
    axios.post(
            `${cors}http://18.219.58.75:3000/api/getUser`,
            userData)
        .then(res => {
            // let caffeDetails;
            if (res.data.valid) {
                axios.post(`${cors}http://18.219.58.75:3000/api/getcafedetails`, {
                    caffeID: userData.caffeID
                }).then(response => {
                    dispatch(setCurrentUser(res.data.valid, userData.caffeID,
                        response.data))
                    let tempData = {
                        login: res.data.valid,
                        caffeID: userData.caffeID,
                        caffeDetails: response.data
                    }
                    dispatch({
                        type: GET_ERROR,
                        payload: ""
                    })
                    localStorage.setItem('login', JSON.stringify(tempData))
                })
            } else {
                dispatch(setCurrentUser(false, null,
                    null))
                dispatch({
                    type: GET_ERROR,
                    payload: res.data.valid
                })
            }
        }).catch(err => {
            dispatch({
                type: GET_ERROR,
                payload: err.response.data
            })
        })
}

export const logout = () => dispatch => {
    localStorage.removeItem('login')
    dispatch({
        type: LOGOUT
    })
}