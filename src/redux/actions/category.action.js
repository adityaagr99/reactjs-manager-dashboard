import axios from "axios";

import {
  GET_ERROR,
  GET_ALL_CATEGORY,
  TOGGLE_FOOD_ITEM,
  SET_PENDING,
  GET_ALL_ADDON,
  TOGGLE_ADDON,
  GET_ALL_CUSTOMER_ORDER,

} from "../types";
import {
  setLoading
} from "./auth.action";


const cors = "https://cors-anywhere.herokuapp.com/";

//get all category
export const getAllCategory = data => dispatch => {
  dispatch(setLoading());
  axios
    .post(`${cors}http://18.219.58.75:3000/api/getCategoryName`, data)
    .then(res => {
      let tempData = res.data.Items;
      tempData.forEach(e => {
        let dataFoodItem = {
          caffeID: data.caffeID,
          foodcategory: e.foodcategory
        };
        axios
          .post(`${cors}http://18.219.58.75:3000/api/getFoodInfo`,
            dataFoodItem)
          .then(response => {
            e.data = response.data.Items;
            dispatch({
              type: GET_ALL_CATEGORY,
              payload: tempData
            });
          });
      });
    })
    .catch(err => {
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      });
    });
};

// disable food item
export const toggleFoodItem = (type, data) => dispatch => {
  dispatch(setPending())
  axios.post(
    `${cors}http://18.219.58.75:3000/api/${type==="disable"?"unavailMenu":"availMenu"}`,
    data).then(res => {
    dispatch({
      type: TOGGLE_FOOD_ITEM,
      payload: data.foodID
    })
  })
}

// disable food item
export const toggleAddonItem = (type, data) => dispatch => {
  dispatch(setPending())
  axios.post(
    `${cors}http://18.219.58.75:3000/api/${type==="disable"?"unavailAddon":"availAddon"}`,
    data).then(res => {
    dispatch({
      type: TOGGLE_ADDON,
      payload: data.addonid
    })
  })
}

export const setPending = () => ({
  type: SET_PENDING
})


// get all addon
export const getALlAdon = data => dispatch => {
  axios.post(`${cors}http://18.219.58.75:3000/api/getAddonInfo`, data).then(
    res => {
      dispatch({
        type: GET_ALL_ADDON,
        payload: res.data
      })
    }).catch(err => {
    dispatch({
      type: GET_ERROR,
      payload: err.response.data
    })
  })
}

// get all customer order
export const getCustomerOrder = (data, foodname) => ({
  type: GET_ALL_CUSTOMER_ORDER,
  payload: {
    data,
    foodname
  }
})
