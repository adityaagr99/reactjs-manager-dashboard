import axios from 'axios';
import {
  GET_ALL_ORDER,
  DELETE_FOOD_ITEM,
  GET_ERROR,
  ACCEPT_PAYMENT,
  GET_ORDER_BILL,
  ADD_OTHER_ITEM,
  REMOVE_CUSTOM_ITEM,
  GET_OTHER_ITEM,
  SET_TABLE_ID_TIME,
} from '../types';
import {
  setLoading
} from './auth.action';
const cors = "https://cors-anywhere.herokuapp.com/"

// get all order
export const getAllOrder = (data, isUpdate = true) => dispatch => {
  if (!isUpdate) {
    dispatch(setLoading())
  }
  axios.post(
      `${cors}http://18.219.58.75:3000/api/getOrdermanager`,
      data)
    .then(res => {
      dispatch({
        type: GET_ALL_ORDER,
        payload: res.data
      })
    }).catch(err => {
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })

    })
}

//get info to print bill
export const getOrderTobill = (data, tableidtimestamp) => dispatch => {
  dispatch(setLoading())
  axios.post(
      `${cors}http://18.219.58.75:3000/api/getOrdermanager`,
      data)
    .then(res => {
      dispatch({
        type: GET_ORDER_BILL,
        payload: res.data.filter(item => item.tableidtimestamp ===
          tableidtimestamp)
      })
    }).catch(err => {
      return dispatch({
        type: GET_ERROR,
        payload: err
      })
    })
}

// add other food item (add custom)
export const addOtherItem = (data, tableidtimestamp, order_identifier) => ({
  type: ADD_OTHER_ITEM,
  payload: {
    data,
    tableidtimestamp,
    order_identifier
  }
})

// delete food item
export const deleteOrderItem = (data) => dispatch => {
  axios.post(
    `${cors}http://18.219.58.75:3000/api/deleteOrder`,
    data).then(res => {
    let resData = {
      orders: res.data.deleteOrder.Attributes.orders,
      tableidtimestamp: data
        .tableidtimestamp,
      order_identifier: data.order_identifier,
      foodid: data.foodid
    }
    dispatch({
      type: DELETE_FOOD_ITEM,
      payload: resData
    })
  })
}

//accept paymen
// "checkPayment": "success"
export const acceptPayment = (data, tableidtimestamp) => dispatch => {
  axios.post(`${cors}http://18.219.58.75:3000/api/checkPayment`, data)
    .then(
      res => {
        dispatch({
          type: ACCEPT_PAYMENT,
          payload: {
            check: res.data.checkPayment,
            tableidtimestamp
          }
        })
      }).catch(err => {
      dispatch({
        type: GET_ERROR,
        payload: err.response.data
      })
    })
}

// remove custom item
export const removeCustomItemAction = (id, tableidtimestamp) => ({
  type: REMOVE_CUSTOM_ITEM,
  payload: {
    id,
    tableidtimestamp
  }
})

// get other item
export const getOrtherItem = data => dispatch => {
  // console.log(data);
  dispatch({
    type: SET_TABLE_ID_TIME,
    payload: data.tableidtimestamp
  })
  dispatch({
    type: GET_OTHER_ITEM,
    payload: data
  })
}
