import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import routes from "./routes";
import withTracker from "./withTracker";

import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/shard-dashboard.css";
import { Provider } from "react-redux";
import store from "./Store";
import { setCurrentUser } from "./redux/actions/auth.action";

//check login
if (localStorage.login) {
  let localData = JSON.parse(localStorage.login);
  if (localData.login === true) {
    store.dispatch(
      setCurrentUser(true, localData.caffeID, localData.caffeDetails)
    );
  }
}

export default () => (
  <Provider store={store}>
    <Router basename={process.env.REACT_APP_BASENAME || ""}>
      <Switch>
        {routes.map((route, index) => {
          return (
            <Route
              key={index}
              path={route.path}
              exact={route.exact}
              component={withTracker(props => {
                return route.layout ? (
                  <route.layout {...props}>
                    <route.component {...props} />
                  </route.layout>
                ) : (
                  <route.component {...props} />
                );
              })}
            />
          );
        })}
      </Switch>
    </Router>
  </Provider>
);
