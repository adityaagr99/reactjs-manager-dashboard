import React from "react";
import {
  Redirect
} from "react-router-dom";

// Layout Types
import {
  DefaultLayout
} from "./layouts";

// Route Views
import Errors from "./views/Errors";
import Login from "./views/Login";
import OrderManager from "./views/OrderManager";
import EnableDisableFoodItem from "./views/Enable-DisableFoodItem";
import PrintBill from "./views/PrintBIll";
import AddCustomer from "./components/add-customer/AddCustomer";


export default [{
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => < Redirect to = "/order-manager" / >
  },
  {
    path: "/errors",
    layout: DefaultLayout,
    component: Errors
  },
  {
    path: "/login",
    component: Login
  },
  {
    path: '/order-manager',
    layout: DefaultLayout,
    component: OrderManager
  },
  {
    path: '/enable-food-item',
    layout: DefaultLayout,
    component: EnableDisableFoodItem
  },
  {
    path: '/print-bill/:caffeid/:discount/:tableidtimestamp',
    component: PrintBill
  },

  {
    path: '/add-customer',
    component: AddCustomer,
    layout: DefaultLayout
  },
  {
    layout: DefaultLayout,
    component: Errors
  }
];
