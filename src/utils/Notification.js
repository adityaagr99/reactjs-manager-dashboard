import React, { Component } from "react";
import ReactNotification from "react-notifications-component";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { connect } from "react-redux";

class Notification extends Component {
  state = {
    play: false
  };
  audio = new Audio("/iphone_notification.mp3");

  componentDidMount() {
    // this.audio.play();
    store.addNotification({
      title: "Wonderful!",
      message: this.props.notification.mess
        ? this.props.notification.mess
        : "aa",
      type:
        this.props.notification.type === "NEW"
          ? "default"
          : this.props.notification.type === "UPDATED-PAYMENT"
          ? "warning"
          : "success",
      insert: "bot",
      container: "bottom-right",
      animationIn: ["animated", "fadeIn"],
      animationOut: ["animated", "fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true
      },
      width: 300
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(nextProps);
    console.log(this.props);

    // console.log(nextState);
    console.log(
      nextProps.notification.mess !== "" ||
        nextProps.notification.mess !== this.props.notification.mess
    );

    if (
      nextProps.notification.mess !== "" ||
      nextProps.notification.mess !== this.props.notification.mess
    ) {
      this.audio.play();
      store.addNotification({
        title: "Wonderful!",
        message: nextProps.notification.mess
          ? nextProps.notification.mess
          : "aa",
        type:
          nextProps.notification.type === "NEW"
            ? "default"
            : nextProps.notification.type === "UPDATED-PAYMENT"
            ? "warning"
            : "success",
        insert: "bot",
        container: "bottom-right",
        animationIn: ["animated", "fadeIn"],
        animationOut: ["animated", "fadeOut"],
        dismiss: {
          duration: 5000
          // onScreen: true
        },
        width: 300
      });
      return true;
    } else return false;
  }

  render() {
    if (this.props.notification.mess !== "") {
      return (
        <div>
          <ReactNotification />
        </div>
      );
    }
    return null;
  }
}

const mapStateToProps = state => ({
  notification: state.notification
});

export default connect(mapStateToProps, {})(Notification);
