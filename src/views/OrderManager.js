import React, { Component } from "react";
import { Card, CardHeader, Row, Container } from "shards-react";
import OrderItem from "../components/order/OrderItem";
import LoadingScreen from "../utils/LoadingScreen";
import { connect } from "react-redux";
import { getAllOrder } from "../redux/actions/order.action";
import Notification from "../utils/Notification";
import { newNotification } from "../redux/actions/notification.action";
import { Consumer } from "sqs-consumer";
import AWS from "aws-sdk";
import moment from "moment";
AWS.config.update({
  region: "us-east-2",
  accessKeyId: "AKIAIIOHEBFU3GXIGHVA",
  secretAccessKey: "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"
});
class OrderManager extends Component {
  renderOrderItem = (loading, orderlist, err) => {
    if (loading === true && err === "") return <LoadingScreen />;
    if (err !== "")
      return <div className="bg-light card-header card w-100">{err}</div>;
    if (!loading && orderlist.length === 0 && err === "")
      return (
        <div className="text-fiord-blue w-100 text-center">
          You have no order!!
        </div>
      );
    if (orderlist.length > 0)
      return orderlist.map((item, i) => <OrderItem orderItem={item} key={i} />);
  };

  componentDidMount() {
    let data = {
      caffeID: this.props.auth.caffeID,
      currentDate: moment().format("DD/MM/YYYY")
    };
    this.props.getAllOrder(data, false);
  }
  UNSAFE_componentWillReceiveProps() {
    // sqs
    const app = Consumer.create({
      queueUrl: `https://sqs.us-east-2.amazonaws.com/598593389908/manager_queue_${this.props.auth.caffeID}`,
      handleMessage: async message => {
        let data = {
          caffeID: this.props.auth.caffeID,
          currentDate: moment().format("DD/MM/YYYY")
        };
        console.log(message);

        if (
          message.Body.indexOf("NEW CLIENT") !== -1 ||
          message.Body.indexOf("PAYMENT-Cash") !== -1 ||
          message.Body.indexOf("PAYMENT-Card") !== -1 ||
          message.Body.indexOf("NEW_ORDER") !== -1 ||
          message.Body.indexOf("ORDER_UPDATED") !== -1 ||
          message.Body.indexOf("ISSUE_RAISED") !== -1
        ) {
          this.props.newNotification(message.Body);
          this.props.getAllOrder(data, true);
        }
      },
      sqs: new AWS.SQS()
    });

    app.on("error", err => {
      console.error(err.message);
    });

    app.on("processing_error", err => {
      console.error(err.message);
    });

    app.on("timeout_error", err => {
      console.error(err.message);
    });

    app.start();
  }

  render() {
    const { order, errors } = this.props;
    return (
      <Container fluid className="main-content-container px-4">
        <div className="page-header py-4 no-gutters row">
          <div className="text-sm-left mb-3 text-center text-md-left mb-sm-0 col-12 col-sm-4">
            <span className="text-uppercase page-subtitle"> Hello </span>
            <h3 className="page-title"> Mr.Mangager </h3>
          </div>
        </div>
        <Row>
          <Card className="mb-4 overflow-hidden w-100">
            <CardHeader className="bg-light" onClick={this.toggle}>
              <table className="table mb-0">
                <thead className="">
                  <tr>
                    <th
                      scope="col"
                      className="border-0"
                      style={{
                        width: "15%"
                      }}
                    >
                      <div className="text-muted text-uppercase"> Name </div>
                    </th>
                    <th
                      scope="col"
                      className="border-0"
                      style={{
                        width: "15%"
                      }}
                    >
                      <div className="text-muted text-uppercase">Table ID</div>
                    </th>
                    <th
                      scope=""
                      className="border-0"
                      style={{
                        width: "50%"
                      }}
                    >
                      <div className="text-muted text-uppercase">
                        Payment mode / Complaint
                      </div>
                    </th>
                    <th scope="col" className="border-0">
                      <div className="text-muted text-uppercase"> Total </div>
                    </th>
                  </tr>
                </thead>
              </table>
            </CardHeader>
          </Card>
          {this.renderOrderItem(order.loading, order.orderlist, errors.text)}
        </Row>
        <Notification> </Notification>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order,
  notification: state.notification,
  errors: state.errors
});

export default connect(mapStateToProps, {
  getAllOrder,
  newNotification
})(OrderManager);
