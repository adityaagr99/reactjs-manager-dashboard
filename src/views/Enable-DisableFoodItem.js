import React, { Component } from "react";
import { Container, Row } from "shards-react";
import Category from "../components/enable-disable -fooditem/Category";
import { connect } from "react-redux";
import { getAllCategory, getALlAdon } from "../redux/actions/category.action";
import LoadingScreen from "../utils/LoadingScreen";
import Addon from "../components/enable-disable -fooditem/Addon";
class EnableDisableFoodItem extends Component {
  renderCategory = (list, loading, err) => {
    if (loading && err === "") return <LoadingScreen />;
    if (err !== "")
      return <div className="bg-light card-header  card w-100">{err}</div>;
    if (list.length === 0)
      return (
        <div className="text-fiord-blue w-100 text-center">
          You have no Item
        </div>
      );
    if (list.length > 0)
      return list.map((item, i) => <Category categoryItem={item} key={i} />);
  };

  componentDidMount() {
    const data = {
      caffeID: this.props.auth.caffeID
    };
    this.props.getAllCategory(data);
    this.props.getALlAdon(data);
  }
  render() {
    const { category, errors } = this.props;
    return (
      <Container fluid className="main-content-container px-4">
        <div className="page-header py-4 no-gutters row">
          <div className="text-sm-left mb-3 text-center text-md-left mb-sm-0 col-12 col-sm-4">
            <span className="text-uppercase page-subtitle">Enable Disable</span>
            <h3 className="page-title">Food Item</h3>
          </div>
        </div>
        <Row>
          {this.renderCategory(category.list, category.loading, errors.text)}
          {!category.loading ? <Addon addonList={category.addonList} /> : null}
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  category: state.category,
  errors: state.errors
});

export default connect(mapStateToProps, { getAllCategory, getALlAdon })(
  EnableDisableFoodItem
);
