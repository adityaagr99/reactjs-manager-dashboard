import React, { Component } from "react";
import {
  Container,
  Card,
  CardBody,
  Button,
  Row,
  Col,
  Form,
  FormInput,
  FormGroup,
  Alert,
  Modal,
  ModalBody
} from "shards-react";
import { connect } from "react-redux";
import { login } from "../redux/actions/auth.action";
import { HashLoader } from "react-spinners";
import "../styles/login.css";
export class Login extends Component {
  state = {
    caffeID: "",
    password: "",
    open: false
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onLogin = () => {
    const userData = {
      caffeID: this.state.caffeID,
      password: this.state.password
    };
    if (this.state.caffeID !== "" && this.state.password !== "") {
      this.props.login(userData);
      this.setState({
        open: true
      });
    }
  };

  toggle = () => {
    this.setState({
      open: !this.state.open
    });
  };

  static getDerivedStateFromProps(props, state) {
    if (props.auth.isAdmin) {
      props.history.push("/order-manager");
      return true;
    } else {
      return null;
    }
  }

  render() {
    const { auth, errors } = this.props;
    return (
      <Container className="py-4">
        <Row className="d-flex justify-content-center pt-5">
          <Col sm="12" lg="6" md="4" className="pt-4">
            <Card
              style={{
                maxWidth: "100%"
              }}
            >
              <CardBody>
                <Form>
                  <FormGroup>
                    <label htmlFor="#caffeID"> CaffeID </label>
                    <FormInput
                      id="#caffeID"
                      placeholder="caffeID"
                      name="caffeID"
                      onChange={this.onChange}
                    />
                  </FormGroup>
                  <FormGroup>
                    <label htmlFor="#password"> Password </label>
                    <FormInput
                      type="password"
                      id="#password"
                      placeholder="Password"
                      name="password"
                      onChange={this.onChange}
                      required
                    />
                    {errors &&
                    errors.text === false &&
                    auth.isAdmin === false ? (
                      <Alert
                        theme="danger"
                        className="mt-3"
                        style={{
                          borderRadius: ".25rem "
                        }}
                      >
                        Inncorrect Login Credentials, Please contact FRINKS TEAM
                        incase you forgot it
                      </Alert>
                    ) : null}
                  </FormGroup>
                </Form>
                <Button
                  onClick={this.onLogin}
                  disabled={auth.loading ? true : false}
                >
                  Login
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <>
          <Modal
            open={auth.loading}
            // toggle={this.toggle}
            centered
            modalClassName="login__modal"
          >
            <ModalBody>
              <HashLoader
                css={`
                  display: block;
                  margin: 0 auto;
                `}
                sizeUnit={"px"}
                size={150}
                color={"#007bff"}
                loading={auth.loading}
              />
            </ModalBody>
          </Modal>
        </>
      </Container>
    );
  }
}

const mapstateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapstateToProps,
  {
    login
  }
)(Login);
