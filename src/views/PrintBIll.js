import React, { Component } from "react";
import styled from "@react-pdf/styled-components";
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  PDFViewer,
  Font,
  PDFDownloadLink
} from "@react-pdf/renderer";
import { connect } from "react-redux";
import { getOrderTobill } from "../redux/actions/order.action";
import moment from "moment";
import LoadingScreen from "../utils/LoadingScreen";
import "../styles/printbill.css";

// Register font
Font.register({
  family: "Monospaced",
  src: "https://fonts.googleapis.com/css?family=Space+Mono&display=swap"
});

const styles = StyleSheet.create({
  section: {
    margin: 10,
    padding: 20
  },
  all: {
    width: "50%",
    height: "95vh",
    fontFamily: "Monospaced",
    left: "50%"
  },
  heading: {
    textAlign: "center",
    margin: "0 0 10px 0"
  },
  line: {
    width: "100%",
    height: "1px",
    backgroundColor: "#888"
  },
  header: {
    display: "flex",
    flexDirection: "column"
  }
});
const TableInfo = styled.View`
  padding: 10px;
  display: flex;
  flex-direction: row;
  font-size: 12px;
`;
const TableInfoItem = styled.Text`
  width: 50%;
  flex-grow: 1;
  font-size: 12px;
`;
const TableInfoItem60 = styled.Text`
  width: 60%;
  flex-grow: 1;
  font-size: 12px;
`;
const TableInfoItem40 = styled.Text`
  width: 40%;
  flex-grow: 1;
  font-size: 12px;
`;
const TableInfoItem40R = styled.Text`
  width: 40%;
  flex-grow: 1;
  text-align: right;
  padding: 0 15px 0 0;
  font-size: 12px;
`;
const TableInfoItem20 = styled.Text`
  width: 20%;
  flex-grow: 1;
  font-size: 12px;
`;
const Footer = styled.View`
  margin: 10px 0 0 0;
`;

const Header = styled.View`
  margin: 0 0 10px 0;
`;

class PrintBill extends Component {
  convertTime = data => {
    let y = new Date(parseInt(data.slice(data.indexOf("---") + 3)));
    let x = moment(y, "YYYY-MM-DDDD").format("DD/MM/YYYY, h:mm:ss ");
    return x;
  };

  componentDidMount() {
    let data = {
      caffeID: this.props.match.params.caffeid,
      currentDate: moment().format("DD/MM/YYYY")
    };
    this.props.getOrderTobill(data, this.props.match.params.tableidtimestamp);
  }

  renderBill = (x, auth, order) => {
    return (
      <PDFViewer style={styles.all}>
        <Document>
          <Page wrap={true}>
            <View style={styles.section}>
              {/* bill info */}
              <TableInfo style={styles.header}>
                <Text style={styles.heading}>
                  {auth.caffeDetails.caffename}
                </Text>
                <Text style={styles.heading}>
                  {auth.caffeDetails.billing_address.slice(
                    0,
                    auth.caffeDetails.billing_address.indexOf("@")
                  )}
                </Text>
                <Text style={styles.heading}>
                  {auth.caffeDetails.billing_address.slice(
                    auth.caffeDetails.billing_address.indexOf("@") + 1
                  )}
                </Text>
                <Text style={styles.heading}>
                  {auth.caffeDetails.gst_number}
                </Text>
                <Text style={styles.heading}>
                  {auth.caffeDetails.caffephone}
                </Text>
                <Text style={styles.line}></Text>
              </TableInfo>
              <TableInfo>
                <TableInfoItem>
                  Table No: {order.orderlist[0].tableid}
                </TableInfoItem>
              </TableInfo>
              <TableInfo>
                <TableInfoItem>
                  Bill No: {order.orderlist[0].bill_number}
                </TableInfoItem>
              </TableInfo>
              <TableInfo>
                <TableInfoItem>
                  {`Date - ${this.convertTime(
                    order.orderlist[0].tableidtimestamp
                  )}`}
                </TableInfoItem>
              </TableInfo>
              <Text style={styles.line}></Text>
              {/* food item list */}
              {order.orderlist[0].orders.map((order, i) => (
                <View key={i}>
                  <TableInfo>
                    <TableInfoItem40>Desciption</TableInfoItem40>
                    <TableInfoItem20>Rate</TableInfoItem20>
                    <TableInfoItem20>Qty</TableInfoItem20>
                    <TableInfoItem20>Price</TableInfoItem20>
                  </TableInfo>
                  {order.food_items.map((item, k) => (
                    <React.Fragment key={k}>
                      <TableInfo>
                        <TableInfoItem40>{item.food_item}</TableInfoItem40>
                        <TableInfoItem20>{item.unit_price}</TableInfoItem20>
                        <TableInfoItem20>{item.quantity}</TableInfoItem20>
                        <TableInfoItem20>
                          {item.unit_price * item.quantity}
                        </TableInfoItem20>
                      </TableInfo>
                      {item.addon &&
                        item.addon.map((e, i) => (
                          <TableInfo key={k}>
                            <TableInfoItem40R>{e.addonname}</TableInfoItem40R>
                            <TableInfoItem20>{e.addonprice}</TableInfoItem20>
                            <TableInfoItem20>{item.quantity}</TableInfoItem20>
                            <TableInfoItem20>
                              {e.addonprice * item.quantity}
                            </TableInfoItem20>
                          </TableInfo>
                        ))}
                    </React.Fragment>
                  ))}
                  <Text style={styles.line}></Text>
                </View>
              ))}
              {/* subtotal */}
              <View>
                <TableInfo>
                  <TableInfoItem60></TableInfoItem60>
                  <TableInfoItem20>Subtotal</TableInfoItem20>
                  <TableInfoItem20>{`${x} Rs`}</TableInfoItem20>
                </TableInfo>
                <TableInfo>
                  <TableInfoItem60></TableInfoItem60>
                  <TableInfoItem20>Discount</TableInfoItem20>
                  <TableInfoItem20>{`${this.props.match.params.discount} %`}</TableInfoItem20>
                </TableInfo>
                <TableInfo>
                  <TableInfoItem60></TableInfoItem60>
                  <TableInfoItem20>Total</TableInfoItem20>
                  <TableInfoItem20>
                    {`${(x * (100 - this.props.match.params.discount)) /
                      100} Rs`}
                  </TableInfoItem20>
                </TableInfo>
                <TableInfo>
                  <TableInfoItem60></TableInfoItem60>
                  <TableInfoItem20>GST</TableInfoItem20>
                  <TableInfoItem20>{`${(((x *
                    (100 - this.props.match.params.discount)) /
                    100) *
                    5) /
                    100} Rs`}</TableInfoItem20>
                </TableInfo>
                <TableInfo>
                  <TableInfoItem60></TableInfoItem60>
                  <TableInfoItem20>Grand Total</TableInfoItem20>
                  <TableInfoItem20>
                    {`${(x * (100 - this.props.match.params.discount)) / 100 +
                      (((x * (100 - this.props.match.params.discount)) / 100) *
                        5) /
                        100} Rs`}
                  </TableInfoItem20>
                </TableInfo>
                <Text style={styles.line}></Text>
              </View>
              {/* thank you */}
              <TableInfo style={styles.header}>
                <Text style={styles.heading}>Thank you</Text>
                <Text style={styles.heading}> Please visit again</Text>
              </TableInfo>
            </View>
          </Page>
        </Document>
      </PDFViewer>
    );
  };

  renderBill1 = (x, auth, order) => {
    return (
      <Document>
        <Page wrap={true}>
          <View style={styles.section}>
            {/* bill info */}
            <Header>
              <Text style={styles.heading}>{auth.caffeDetails.caffename}</Text>
              <Text style={styles.heading}>
                {auth.caffeDetails.billing_address.slice(
                  0,
                  auth.caffeDetails.billing_address.indexOf("@")
                )}
              </Text>
              <Text style={styles.heading}>
                {auth.caffeDetails.billing_address.slice(
                  auth.caffeDetails.billing_address.indexOf("@") + 1
                )}
              </Text>
              <Text style={styles.heading}>{auth.caffeDetails.gst_number}</Text>
              <Text style={styles.heading}>{auth.caffeDetails.caffephone}</Text>
              <Text style={styles.line}></Text>
            </Header>
            <TableInfo>
              <TableInfoItem>
                Table No: {order.orderlist[0].tableid}
              </TableInfoItem>
            </TableInfo>
            <TableInfo>
              <TableInfoItem>
                Bill No: {order.orderlist[0].bill_number}
              </TableInfoItem>
            </TableInfo>
            <TableInfo>
              <TableInfoItem>
                {`Date - ${this.convertTime(
                  order.orderlist[0].tableidtimestamp
                )}`}
              </TableInfoItem>
            </TableInfo>
            <Text style={styles.line}></Text>
            {/* food item list */}
            {order.orderlist[0].orders.map((order, i) => (
              <View key={i}>
                <TableInfo>
                  <TableInfoItem40>Desciption</TableInfoItem40>
                  <TableInfoItem20>Rate</TableInfoItem20>
                  <TableInfoItem20>Qty</TableInfoItem20>
                  <TableInfoItem20>Price</TableInfoItem20>
                </TableInfo>
                {order.food_items.map((item, k) => (
                  <React.Fragment key={k}>
                    <TableInfo>
                      <TableInfoItem40>{item.food_item}</TableInfoItem40>
                      <TableInfoItem20>{item.unit_price}</TableInfoItem20>
                      <TableInfoItem20>{item.quantity}</TableInfoItem20>
                      <TableInfoItem20>
                        {item.unit_price * item.quantity}
                      </TableInfoItem20>
                    </TableInfo>
                    {item.addon &&
                      item.addon.map((e, i) => (
                        <TableInfo key={k}>
                          <TableInfoItem40R>{e.addonname}</TableInfoItem40R>
                          <TableInfoItem20>{e.addonprice}</TableInfoItem20>
                          <TableInfoItem20>{item.quantity}</TableInfoItem20>
                          <TableInfoItem20>
                            {e.addonprice * item.quantity}
                          </TableInfoItem20>
                        </TableInfo>
                      ))}
                  </React.Fragment>
                ))}
                <Text style={styles.line}></Text>
              </View>
            ))}
            {/* subtotal */}
            <View>
              <TableInfo>
                <TableInfoItem60></TableInfoItem60>
                <TableInfoItem20>Subtotal</TableInfoItem20>
                <TableInfoItem20>{`${x} Rs`}</TableInfoItem20>
              </TableInfo>
              <TableInfo>
                <TableInfoItem60></TableInfoItem60>
                <TableInfoItem20>Discount</TableInfoItem20>
                <TableInfoItem20>{`${this.props.match.params.discount} %`}</TableInfoItem20>
              </TableInfo>
              <TableInfo>
                <TableInfoItem60></TableInfoItem60>
                <TableInfoItem20>Total</TableInfoItem20>
                <TableInfoItem20>
                  {`${(x * (100 - this.props.match.params.discount)) / 100} Rs`}
                </TableInfoItem20>
              </TableInfo>
              <TableInfo>
                <TableInfoItem60></TableInfoItem60>
                <TableInfoItem20>GST</TableInfoItem20>
                <TableInfoItem20>{`${(((x *
                  (100 - this.props.match.params.discount)) /
                  100) *
                  5) /
                  100} Rs`}</TableInfoItem20>
              </TableInfo>
              <TableInfo>
                <TableInfoItem60></TableInfoItem60>
                <TableInfoItem20>Grand Total</TableInfoItem20>
                <TableInfoItem20>
                  {`${(x * (100 - this.props.match.params.discount)) / 100 +
                    (((x * (100 - this.props.match.params.discount)) / 100) *
                      5) /
                      100} Rs`}
                </TableInfoItem20>
              </TableInfo>
              <Text style={styles.line}></Text>
            </View>
            {/* thank you */}
            <Footer>
              <Text style={styles.heading}>Thank you</Text>
              <Text style={styles.heading}> Please visit again</Text>
            </Footer>
          </View>
        </Page>
      </Document>
    );
  };

  render() {
    const { auth, order } = this.props;
    let x = 0;
    if (order.orderlist[0]) {
      order.orderlist[0].orders.length !== undefined &&
        order.orderlist[0].orders.forEach(item => {
          if (item.food_items.length > 0) {
            item.food_items.forEach(e => {
              x += e.total;
            });
          }
        });
    }
    if (
      !this.props.order.loading &&
      this.props.auth.isAdmin &&
      this.props.order.orderlist.length > 0 &&
      this.props.order.orderlist[0].orders.length > 0
    ) {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {this.renderBill(x, auth, order)}
          {this.renderBill1(x, auth, order) && (
            <>
              <PDFDownloadLink
                className="btn btn-primary"
                document={this.renderBill1(x, auth, order)}
                fileName="somename.pdf"
              >
                {({ blob, url, loading, error }) =>
                  loading ? "Loading document..." : "Download now!"
                }
              </PDFDownloadLink>
              {/* <button
                href=""
                className="btn btn-success"
                onClick={() => window.print()}
              >
                click
              </button> */}
            </>
          )}
        </div>
      );
    } else if (
      this.props.order.orderlist[0] &&
      this.props.order.orderlist[0].orders.length === undefined
    ) {
      return <div style={{ textAlign: "center" }}>no item</div>;
    } else {
      return <LoadingScreen />;
    }
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order
});

export default connect(mapStateToProps, { getOrderTobill })(PrintBill);
