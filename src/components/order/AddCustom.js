import React, { Component } from "react";
import { Button } from "shards-react";
import { connect } from "react-redux";
import { addOtherItem, getOrtherItem } from "../../redux/actions/order.action";

export class AddCustom extends Component {
  state = {
    isOpen: false,
    item_name: "",
    item_price: 0,
    item_qty: 0
  };

  toggleAddCustom = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addCustom = (tableidtimestamp, order_identifier) => {
    let data = {
      foodid: `addcustom-${Date.now()}`,
      addon: [],
      cgst: (this.state.item_price * this.state.item_qty * 5) / 200,
      food_category: "other",
      food_hashtag: " ",
      food_item: this.state.item_name,
      food_type: " ",
      quantity: parseInt(this.state.item_qty),
      sgst: (this.state.item_price * this.state.item_qty * 5) / 200,
      total: this.state.item_price * this.state.item_qty,
      unit_price: parseInt(this.state.item_price)
    };

    if (
      this.state.item_name !== "" &&
      this.state.item_price !== 0 &&
      this.state.item_qty !== 0
    ) {
      this.props.addOtherItem(data, tableidtimestamp, order_identifier);
      this.setState({ item_name: "", item_price: 0, item_qty: 0 });
    }
  };

  render() {
    const { item_name, item_price, item_qty, isOpen } = this.state;
    const { tableidtimestamp, order_identifier } = this.props;
    return !isOpen ? (
      <Button className="mb-2" onClick={() => this.toggleAddCustom()}>
        <i className="material-icons ">add_circle_outline</i>
        Add Custom
      </Button>
    ) : (
      <form className="form-group">
        <div className="form-row">
          <div className="col-sm-5">
            <input
              placeholder="Item name"
              required
              className={`form-control addcustom_form ${
                item_name !== "" ? "is-valid" : "is-invalid"
              }`}
              value={item_name}
              name="item_name"
              onChange={e => this.onChange(e)}
            />
          </div>
          <div className="col-sm-3">
            <input
              placeholder="Item price"
              required
              className={`form-control addcustom_form ${
                item_price !== 0 ? "is-valid" : "is-invalid"
              }`}
              value={item_price}
              name="item_price"
              type="number"
              min="0"
              onChange={e => this.onChange(e)}
            />
          </div>
          <div className="col-sm-2">
            <input
              type="number"
              placeholder="Item quantity"
              required
              className={`form-control addcustom_form ${
                item_qty !== 0 ? "is-valid" : "is-invalid"
              }`}
              value={item_qty}
              name="item_qty"
              min="0"
              onChange={e => this.onChange(e)}
            />
          </div>
          <button
            type="button"
            className="btn btn-success col-sm-1 "
            onClick={() => this.addCustom(tableidtimestamp, order_identifier)}
          >
            <i className="material-icons">check</i>
          </button>
          <button
            type="button"
            className="btn btn-danger col-sm-1 "
            onClick={() => this.toggleAddCustom()}
          >
            <i className="material-icons">close</i>
          </button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order
});

export default connect(
  mapStateToProps,
  { addOtherItem, getOrtherItem }
)(AddCustom);
