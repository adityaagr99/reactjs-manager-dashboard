import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalHeader,
  Button,
  FormSelect,
  FormInput
} from "shards-react";
import "../../styles/order.css";
import { connect } from "react-redux";
import {
  acceptPayment,
  removeCustomItemAction
} from "../../redux/actions/order.action";
import { Link } from "react-router-dom";
import AddCustom from "./AddCustom";
import moment from "moment";
class Billmodal extends Component {
  state = {
    discount: 0,
    openDiscount: false,
    paymentMethod: "",
    openAddCustom: false,
    isPrinted: false,
    item_name: "",
    item_price: 0,
    item_qty: 0
  };

  toggle = () => {
    this.props.toggle();
  };

  openDiscount = () => {
    this.setState({ openDiscount: !this.state.openDiscount });
  };

  onChangeDiscount = e => {
    if (e.target.value < 0) {
      this.setState({ discount: 0 });
    } else if (e.target.value > 100) {
      this.setState({ discount: 100 });
    } else {
      this.setState({ discount: e.target.value });
    }
  };

  onChangePayment = e => {
    this.setState({ paymentMethod: e.target.value });
  };

  getOrderlist = (data, tableidtimestamp) => {
    let tempOders = [];
    data.forEach(order => {
      if (order.tableidtimestamp === tableidtimestamp) {
        tempOders = order.orders;
      }
    });
    if (tempOders.length > 0) {
      tempOders[tempOders.length - 1].food_items = tempOders[
        tempOders.length - 1
      ].food_items.filter(e => e.foodid.search("addcustom") === -1);
    }

    return tempOders;
  };

  getOtheritems = (data, tableidtimestamp) => {
    let temp = [];
    data.forEach(item => {
      if (item.tableidtimestamp === tableidtimestamp) {
        item.orders.length > 0 &&
          item.orders.forEach(e => {
            e.food_items.forEach(k => {
              if (k.foodid.search("addcustom") !== -1) {
                temp = [...temp, k];
                temp.forEach(item => {
                  item.foodid = "";
                });
              }
            });
          });
      }
    });
    return temp;
  };

  onPayment = tableidtimestamp => {
    let other_order_identifier = this.calcOderIdentifier(
      this.props.order.orderlist,
      tableidtimestamp
    );
    let order_time = moment().format("DD/MM/YYYY hh:mm:ss");
    let orders = [];
    let other_items_in_orders = {
      food_items: this.getOtheritems(
        this.props.order.orderlist,
        tableidtimestamp
      ),
      is_placed: true,
      order_identifier: other_order_identifier,
      order_time
    };
    let temp_orders = this.getOrderlist(
      this.props.order.orderlist,
      tableidtimestamp
    );
    temp_orders.length > 0 &&
      temp_orders.forEach(e => {
        orders = [...orders, e];
      });
    orders = [...orders, other_items_in_orders];
    let other_items = [];
    other_items_in_orders.food_items.forEach(e => {
      let tempOtherItem = {};
      tempOtherItem.food_item = e.food_item;
      tempOtherItem.quantity = e.quantity;
      tempOtherItem.unit_price = e.unit_price;
      tempOtherItem.total = e.total;
      other_items = [...other_items, tempOtherItem];
    });
    let data = {
      caffeID: this.props.auth.caffeID,
      tableidtimestamp,
      add_discount: this.state.discount,
      other_items: other_items,
      orders
    };
    if (window.confirm("Confim ?")) {
      this.props.acceptPayment(data, tableidtimestamp);
      this.toggle();
    }
  };

  calcOderIdentifier = (data, id) => {
    let tempdata = data.filter(e => e.tableidtimestamp === id);
    let len = tempdata[0].orders.length + 1;
    let str = "" + len;
    let pad = "0000";
    let ans = `Order${pad.substring(0, pad.length - str.length) + str}`;
    return ans;
  };

  printedBill = () => {
    this.setState({ isPrinted: true });
  };

  renderFoodItemList = data => {
    const { tableidtimestamp } = this.props;
    if (data.length !== undefined)
      return (
        <>
          <table className="table mb-0">
            <thead className="bg-light">
              <tr>
                <th scope="col" className="border-0 pl-0 w-25">
                  Description
                </th>
                <th scope="col" className="border-0 w-25">
                  Rate
                </th>
                <th scope="col" className="border-0 w-25">
                  Qty
                </th>
                <th scope="col" className="border-0 w-25">
                  Price
                </th>
              </tr>
            </thead>
            <tbody>
              {data.map(item =>
                item.food_items.map((e, k) => (
                  <React.Fragment key={k}>
                    <tr>
                      <td className="pl-0">{e.food_item}</td>
                      <td>{e.unit_price}</td>
                      <td>{e.quantity}</td>
                      <td className="d-flex justify-content-between">
                        {e.unit_price * e.quantity}
                        {e.foodid.search("addcustom") !== -1 ? (
                          <Button
                            theme="danger"
                            className="ml-1"
                            size="sm"
                            onClick={() =>
                              this.removeCustomItem(e.foodid, tableidtimestamp)
                            }
                          >
                            <i className="material-icons">
                              remove_circle_outline
                            </i>
                          </Button>
                        ) : (
                          ""
                        )}
                      </td>
                    </tr>
                    {e.addon &&
                      e.addon.map((el, i) => (
                        <tr key={i} className="addon">
                          <td className="pl-0 text-right addon-name">
                            {el.addonname}
                          </td>
                          <td>{el.addonprice}</td>
                          <td>{e.quantity}</td>
                          <td>{el.addonprice * e.quantity}</td>
                        </tr>
                      ))}
                  </React.Fragment>
                ))
              )}
            </tbody>
          </table>
          {/* add custom */}
          <AddCustom
            order_identifier={data[data.length - 1].order_identifier}
            tableidtimestamp={tableidtimestamp}
          />
        </>
      );
  };

  removeCustomItem = (foodid, tableidtimestamp) => {
    this.props.removeCustomItemAction(foodid, tableidtimestamp);
  };

  render() {
    const {
      open,
      bill_number,
      tableId,
      billOrders,
      total,
      tableidtimestamp,
      auth
    } = this.props;
    // console.log(billOrders)
    const { discount, openDiscount, paymentMethod } = this.state;
    return (
      <Modal open={open} toggle={this.toggle} className="bill-modal">
        <ModalHeader className="text-center">
          <div className="font-weight-bold">Payment Confirmation</div>
          <div>Customer Name: {tableId}</div>
          <div>Bill Number: {bill_number}</div>
        </ModalHeader>
        <ModalBody>
          <div className="mb-4">
            <div className="border-bottom">
              <h5 className="m-0 mb-3">Bill detail</h5>
            </div>
            <div className="pl-0 card-body">
              {/* render fooditem list */}
              {this.renderFoodItemList(billOrders)}
              {/* subtotal */}
              <table className="w-100 table">
                <tbody>
                  <tr>
                    <td className="w-25"></td>
                    <td className="w-25"></td>
                    <td className="w-25">SubTotal</td>
                    <td className="w-25">{total}</td>
                  </tr>
                  <tr>
                    <td colSpan="2" className="border-0"></td>
                    <td>
                      {openDiscount ? (
                        <Button
                          theme="success"
                          onClick={() => this.openDiscount()}
                        >
                          <i className="material-icons">done</i>
                        </Button>
                      ) : (
                        <Button onClick={() => this.openDiscount()}>
                          Add discount
                        </Button>
                      )}
                    </td>
                    <td>
                      {openDiscount ? (
                        <FormInput
                          placeholder="Discount percent"
                          onChange={e => this.onChangeDiscount(e)}
                        />
                      ) : (
                        `${discount} ₹`
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="2" className="border-0"></td>
                    <td>Total</td>
                    <td>{total - discount}</td>
                  </tr>
                  <tr>
                    <td colSpan="2" className="border-0"></td>
                    <td>GST</td>
                    <td>{((total - discount)* 5) / 100}</td>
                  </tr>
                  <tr>
                    <td colSpan="2" className="border-0"></td>
                    <td className="font-weight-bold">Grand Total</td>
                    <td>
                      {(total - discount) +
                        ((total - discount)* 5) / 100}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="d-flex mt-3">
              <div className="select-payment w-50">
                <FormSelect
                  style={{ width: "95%" }}
                  value={paymentMethod}
                  onChange={e => this.onChangePayment(e)}
                >
                  <option value="">Select payment method ...</option>
                  <option value="Cash">Cash</option>
                  <option value="Paytm">Paytm</option>
                  <option value="PhonePe">PhonePe</option>
                  <option value="Debit Card">Debit Card</option>
                  <option value="Credit Card">Credit Card</option>
                  <option value="Google Pay">Google Pay</option>
                  <option value="Other">Other</option>
                </FormSelect>
              </div>
              {paymentMethod.trim() !== "" ? (
                <div className="print-bill w-25">
                  <Link
                    to={`/print-bill/${auth.caffeID}/${discount}/${tableidtimestamp}`}
                    target="_blank"
                  >
                    <Button theme="success" onClick={() => this.printedBill()}>
                      Print Bill
                    </Button>
                  </Link>
                </div>
              ) : (
                "Select payment method to print bill"
              )}
              <div className="confirm-payment  w-25">
                <Button onClick={() => this.onPayment(tableidtimestamp)}>
                  Confirm payment
                </Button>
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order
});

export default connect(
  mapStateToProps,
  { acceptPayment, removeCustomItemAction }
)(Billmodal);
