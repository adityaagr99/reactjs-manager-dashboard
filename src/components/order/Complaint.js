import React, { Component } from "react";
import { CardBody, Collapse, Alert } from "shards-react";
class Complaint extends Component {
  state = {
    collapse: true
  };
  toggle = () => {
    this.setState({ collapse: !this.state.collapse });
  };

  renderComplaints = compalints =>
    compalints.map((item, i) => (
      <p className="lead mb-1" key={i}>
        {item.issue}
      </p>
    ));

  render() {
    const { complaints } = this.props;
    return (
      <CardBody
        className="p-0  border rounded"
        style={{ margin: "1.09375rem 1.875rem" }}
      >
        <h5 onClick={this.toggle} className="bg-light p-3 card-title mb-0">
          Complaints
        </h5>
        <Collapse open={this.state.collapse}>
          <div style={{ padding: "1rem" }}>
            {complaints.length === undefined ? (
              <Alert theme="warning" style={{ margin: "1.09375rem 0" }}>
                No complaint !!
              </Alert>
            ) : (
              this.renderComplaints(complaints)
            )}
          </div>
        </Collapse>
      </CardBody>
    );
  }
}
export default Complaint;
