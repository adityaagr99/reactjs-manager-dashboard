import React, { Component } from "react";
import { Card, CardHeader, Collapse, Alert, Button } from "shards-react";
import Order from "./Order";
import Complaint from "./Complaint";
import { connect } from "react-redux";
import { acceptPayment } from "../../redux/actions/order.action";
import Billmodal from "./Billmodal";
class OrderItem extends Component {
  state = {
    collapse: false,
    open: false
  };

  toggle = () => {
    this.setState({ collapse: !this.state.collapse });
  };

  openModal = () => {
    this.setState({ open: !this.state.open });
  };

  render() {
    const { collapse, open } = this.state;
    const { orderItem } = this.props;
    let x = 0;
    orderItem.orders.length !== undefined &&
      orderItem.orders.forEach(item => {
        if (item.food_items.length > 0) {
          item.food_items.forEach(e => {
            x += e.total;
          });
        }
      });

    return (
      <>
        <Card className="mb-4 overflow-hidden w-100 ">
          <CardHeader className="bg-light ">
            <table className="table mb-0">
              <thead className="">
                <tr>
                  <th scope="col" className="border-0" style={{ width: "15%" }}>
                    {orderItem.name}
                  </th>
                  <th scope="col" className="border-0" style={{ width: "15%" }}>
                    {orderItem.tableid}
                  </th>
                  <th scope="col" className="border-0" style={{ width: "50%" }}>
                    {orderItem.paymentmode.trim().length !== 0 ? (
                      <Button
                        theme="success"
                        className="ml-3"
                        onClick={() => this.openModal()}
                      >
                        Print bill and accept payment
                      </Button>
                    ) : null}
                  </th>
                  <th scope="col" className="border-0" style={{ width: "15%" }}>
                    {x} ₹
                  </th>
                  <th
                    scope="col"
                    className="border-0"
                    style={{ cursor: "pointer" }}
                    onClick={this.toggle}
                  >
                    {collapse ? (
                      <i
                        className="material-icons"
                        style={{ fontSize: "25px" }}
                      >
                        keyboard_arrow_up
                      </i>
                    ) : (
                      <i
                        className="material-icons"
                        style={{ fontSize: "25px" }}
                      >
                        keyboard_arrow_down
                      </i>
                    )}
                  </th>
                </tr>
              </thead>
            </table>
          </CardHeader>
          <Collapse open={collapse}>
            {orderItem.orders.length === undefined ||
            orderItem.orders.length === 0 ? (
              <Alert theme="warning" style={{ margin: "1.09375rem 1.875rem" }}>
                No order items !!
              </Alert>
            ) : (
              orderItem.orders.map((item, i) => {
                return (
                  <Order
                    orders={item}
                    key={i}
                    tableidtimestamp={orderItem.tableidtimestamp}
                  ></Order>
                );
              })
            )}
            <hr />
            <Complaint complaints={orderItem.complaints}></Complaint>
          </Collapse>
        </Card>

        {/* =============modal========= */}
        <Billmodal
          open={open}
          tableId={orderItem.tableid}
          tableidtimestamp={orderItem.tableidtimestamp}
          bill_number={orderItem.bill_number ? orderItem.bill_number : null}
          toggle={this.openModal}
          billOrders={orderItem.orders}
          total={x}
        />
      </>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order
});

export default connect(
  mapStateToProps,
  { acceptPayment }
)(OrderItem);
