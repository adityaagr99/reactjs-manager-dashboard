import React, { Component } from "react";
import { CardBody, Collapse, Button, Alert } from "shards-react";
import { connect } from "react-redux";
import { deleteOrderItem } from "../../redux/actions/order.action";

class Order extends Component {
  state = {
    collapse: true
  };

  toggle = () => {
    this.setState({
      collapse: !this.state.collapse
    });
  };

  deleteItem = foodid => {
    const data = {
      caffeID: this.props.auth.caffeID,
      foodid: foodid,
      tableidtimestamp: this.props.tableidtimestamp,
      order_identifier: this.props.orders.order_identifier
    };
    if (window.confirm("Are you sure ??")) {
      this.props.deleteOrderItem(data);
    }
  };

  renderAddon = (e, type) => {
    let addon = "";
    let i = 0;
    let price = 0;
    if (e) {
      e.forEach(element => {
        price += element.addonprice;
        i++;
        if (i === e.length) {
          addon += `${element.addonname}`;
        } else {
          addon += `${element.addonname} & `;
        }
      });
    }
    if (type === "price") {
      return price;
    }
    if (e && e.length !== 0) return ` & ${addon}`;
    return ``;
  };

  renderOrder = food_items => {
    if (food_items) {
      return food_items.map((item, i) => (
        <tr key={i}>
          <td
            style={{
              width: "8%",
              textAlign: "center"
            }}
          >
            {i + 1}
          </td>
          <td
            style={{
              width: "25%"
            }}
          >
            {` ${item.food_item + this.renderAddon(item.addon)}`}
          </td>
          <td> {item.quantity} </td>
          <td>
            {`${item.unit_price + this.renderAddon(item.addon, "price")}`}
          </td>
          <td> {item.total} </td>
          <td>
            <Button theme="danger" onClick={() => this.deleteItem(item.foodid)}>
              Delete
            </Button>
          </td>
        </tr>
      ));
    }
  };

  render() {
    const { orders } = this.props;
    return (
      <CardBody
        className=" p-0  border rounded"
        style={{
          margin: "1.09375rem 1.875rem"
        }}
      >
        <h5 onClick={this.toggle} className="bg-light p-3 card-title mb-0">
          Orders
        </h5>
        <Collapse open={this.state.collapse}>
          <table className="table table-light mb-0">
            <thead className="thead-light">
              <tr>
                <th
                  scope="col"
                  className="border-0"
                  style={{
                    width: "8%",
                    textAlign: "center"
                  }}
                >
                  #
                </th>
                <th
                  scope="col"
                  className="border-0"
                  style={{
                    width: "25%"
                  }}
                >
                  Food Item
                </th>
                <th scope="col" className="border-0">
                  Quantity
                </th>
                <th scope="col" className="border-0">
                  Unit price
                </th>
                <th scope="col" className="border-0">
                  Unit total
                </th>
                <th scope="col" className="border-0">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {orders.food_items.length === 0 ? (
                <tr>
                  <td colSpan="6" className="p-0">
                    <Alert theme="warning" className="mb-0">
                      No food items!!
                    </Alert>
                  </td>
                </tr>
              ) : (
                this.renderOrder(orders.food_items)
              )}
            </tbody>
          </table>
        </Collapse>
      </CardBody>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  order: state.order
});

export default connect(
  mapStateToProps,
  {
    deleteOrderItem
  }
)(Order);
