import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getCustomerOrder,
  getAllCategory
} from "../../redux/actions/category.action";
import { FormCheckbox, FormRadio } from "shards-react";
class FoodCategory extends Component {
  state = {
    foodCategory: "",
    foodname: "",
    isEmpty: false,
    selectedData: [],
    count: 0,
    addOnItem: []
  };

  updateAddOnData = (addonlist, data) => {
    const { addOnItem } = this.state;
    const addonIndex = addOnItem.addon.findIndex(e => e.name === addonlist.name);
    if (addonIndex !== -1) {
      const itemIndex = addOnItem.addon[addonIndex].addonarr.findIndex(e => e.addonname === data.addonname);
      if (addOnItem.addon[addonIndex].select && addOnItem.addon[addonIndex].select === 'multiple') {
        const prevvalue = addOnItem.addon[addonIndex].addonarr[itemIndex].quantity;
        addOnItem.addon[addonIndex].addonarr[itemIndex].quantity = !prevvalue;
      } else if (addOnItem.addon[addonIndex].select && addOnItem.addon[addonIndex].select === 'single') {
        addOnItem.addon[addonIndex].addonarr.forEach(e => e.addonname === data.addonname ? e.quantity = true : false);
      }
    }

    this.setState({
      addOnItem
    });
  };

  renderAddon = data => {
    const { addOnItem } = this.state;
    if (addOnItem && addOnItem.addon.length !== 0) {
      return (
        <React.Fragment>
          {addOnItem.addon.map((e, k) => {
            if (e.select === "multiple") {
              return (
                <div key={k}>
                  <p>{e.name}</p>
                  {e.addonarr.length !== 0 &&
                    e.addonarr.map((m, n) => (
                      <FormCheckbox key={n} onChange={() => this.updateAddOnData(e, m)} name={e.name} checked={m.quantity}>
                        {m.addonname}
                      </FormCheckbox>
                    ))}
                </div>
              );
            } else {
              return (
                <div key={k}>
                  <p className="mb-2">{e.name}</p>
                  {e.addonarr.length !== 0 &&
                    e.addonarr.map((m, n) => (
                      <FormRadio
                        key={n}
                        name={e.name}
                        checked={e.quantity}
                        onChange={() => this.updateAddOnData(e, m)}
                      >
                        {m.addonname}
                      </FormRadio>
                    ))}
                </div>
              );
            }
          })}
          <button onClick={() => this.chose("foodname", addOnItem.foodname, true, addOnItem)}>Add</button>
        </React.Fragment>
      )
    }
  };

  renderFoodItem = data => {
    let tempData = data.filter(e => e.foodcategory === this.state.foodCategory);
    if (tempData[0]) {
      return (
        <div className="food-categories">
          {tempData[0] &&
            tempData[0].data.map((item, k) => (
              <div
                className="food-item rounded border p-2 mb-3"
                key={k}
                onClick={
                  item.addon.length !== 0
                    ? () => this.chose("foodname", item.foodname, false, item, tempData[0].data)
                    : () => this.chose("foodname", item.foodname, true, item, [])
                }
              >
                {item.foodname}
                {item.addon.length !== 0 ? ` *` : null}
              </div>
            ))}
        </div>
      );
    }
  };

  renderFoodCategory = (type, data) => {
    // const { category } = this.props;
    if (this.state.foodCategory === "") {
      return (
        <div className="food-categories">
          {data &&
            data.map((item, k) => (
              <div
                className="food-item rounded border p-2 mb-3"
                key={k}
                onClick={() =>
                  this.chose("category", item.foodcategory, false, null)
                }
              >
                {item.foodcategory}
              </div>
            ))}
        </div>
      );
    } else if (
      (this.state.foodCategory !== "" && this.state.foodname === "") ||
      (this.state.foodCategory !== "" &&
        this.state.foodname !== "" &&
        this.state.isEmpty)
    ) {
      return this.renderFoodItem(data);
    } else if (
      this.state.foodCategory !== "" &&
      this.state.foodname !== "" &&
      !this.state.isEmpty
    ) {
      return this.renderAddon(data);
    }
  };

  chose = (type, data, isEmpty, selectedRecord,addOnItemList = []) => {
    let { foodCategory, foodname, selectedData, addOnItem } = this.state;
    if(addOnItemList && addOnItemList.length > 0){
      addOnItem = addOnItemList.find(
        e => e.foodname === data
      );
    }
    if (type === "category") {
      foodCategory = data;
      // this.setState({ foodCategory: data });
    } else {
      foodname = data;
      // this.setState({ foodname: data });
    }

    if (selectedRecord !== null && isEmpty) {
      const oSelectedItem = { ...selectedRecord, quantity: 1 };
      const existingIndex = selectedData.findIndex(ele => ele.foodid === oSelectedItem.foodid);
      if (existingIndex !== -1) {
        const preQuantity = selectedData[existingIndex].quantity;
        selectedData[existingIndex].quantity = preQuantity + 1;
      } else {
        selectedData = [...selectedData, oSelectedItem];
      }
      /*       this.setState({
              selectedData: newState.selectedData
            }); */
      this.props.getCustomerOrder(
        selectedData,
        selectedData.foodname
      );
    }
    this.setState({
      isEmpty,
      foodCategory,
      foodname,
      selectedData,
      addOnItem
    });
  };

  back = () => {
    if (this.state.foodname !== "") {
      this.setState({ foodname: "", addOnItem: [] });
    }
    if (this.state.foodCategory !== "" && this.state.foodname === "") {
      this.setState({ foodCategory: "", addOnItem: [] });
    }
  };

  componentDidMount() {
    this.props.getAllCategory({ caffeID: this.props.auth.caffeID });
  }

  render() {
    const { foodCategory, foodname, isEmpty } = this.state;
    const { category } = this.props;
    // console.log(this.state.selectedData);
    return (
      <div className="add-custom-food-category">
        <div className="header border-bottom py-2 mb-3">
          Food category {foodCategory !== "" ? ` / ${foodCategory}` : null}
          {foodname !== "" && !isEmpty ? ` / ${foodname}` : null}
          <i
            className="material-icons"
            style={{ color: "#000", cursor: "pointer" }}
            onClick={() => this.back()}
          >
            arrow_back
          </i>
        </div>
        <div className="body">
          {category.loading ? (
            <div className="spinner loading">
              <div className="rect1"></div>
              <div className="rect2"></div>
              <div className="rect3"></div>
              <div className="rect4"></div>
              <div className="rect5"></div>
            </div>
          ) : (
            this.renderFoodCategory("type", category.list)
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  category: state.category
});

export default connect(mapStateToProps, { getCustomerOrder, getAllCategory })(
  FoodCategory
);
