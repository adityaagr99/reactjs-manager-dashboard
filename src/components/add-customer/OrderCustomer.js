import React, { Component } from "react";
import { connect } from "react-redux";
class OrderCustomer extends Component {
  renderOrderList = data => {
    if (data.length !== 0) {
      return data.map((item, k) => (
        <div className="row mx-0" key={k}>
          <div className="col-sm-6 py-2">{item.foodname}</div>
          <div className="col-sm-3 py-2">{item.quantity}</div>
          <div className="col-sm-3 py-2">{item.price}</div>
        </div>
      ));
    }
  };

  render() {
    const { category } = this.props;
    // console.log(category);

    return (
      <div className="">
        <div className="border-bottom order-customer row mx-0">
          <div className="col-sm-6 py-2">Food Item</div>
          <div className="col-sm-3 py-2">Quantity</div>
          <div className="col-sm-3 py-2">Price</div>
        </div>
        <div className="order-customer__body border-bottom">
          {this.renderOrderList(category.customerOrder)}
        </div>
        <div className="order-customer__footer">
          <div className="row mx-0">
            <div className="col-sm-4 py-2">Subtotal</div>
            <div className="col-sm-8 py-2">xxx</div>
          </div>
          <div className="row mx-0">
            <div className="col-sm-4 py-2">Discount</div>
            <div className="col-sm-8 py-2">xxx</div>
          </div>
          <div className="row mx-0">
            <div className="col-sm-4 py-2">Gst</div>
            <div className="col-sm-8 py-2">xxx</div>
          </div>
          <div className="row mx-0">
            <div className="col-sm-4 py-2">Grand Total</div>
            <div className="col-sm-8 py-2">xxx</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category
});
export default connect(mapStateToProps, {})(OrderCustomer);
