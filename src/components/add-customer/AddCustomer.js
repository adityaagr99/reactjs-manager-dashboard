import React, { Component } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  Container,
  Row,
  FormInput,
  FormGroup,
  FormSelect
} from "shards-react";
import "./add-customer.css";
import FoodCategory from "./FoodCategory";
import OrderCustomer from "./OrderCustomer";
class AddCustomer extends Component {
  state = {
    open: true
  };
  toggle = () => {
    this.setState({
      open: !this.state.open
    });
  };
  render() {
    const { open } = this.state;
    return (
      <Container fluid className="main-content-container px-4">
        <div className="page-header py-4 no-gutters row">
          <div className="text-sm-left mb-3 text-center text-md-left mb-sm-0 col-12 col-sm-4">
            <h3 className="page-title">
              Punch takeaway and online orders here
            </h3>
          </div>
        </div>
        <Row className="mx-0">
          <Dropdown
            open={open}
            toggle={this.toggle}
            className="custom-dropdown"
          >
            <DropdownToggle className="mb-3"> + Add Customer</DropdownToggle>
            <DropdownMenu className="custom-dropdown p-3">
              <div className="row ">
                <div className="col-sm-4">
                  <label>Order Type (*)</label>
                  <FormSelect required>
                    <option value="Zomato">Zomato</option>
                    <option value="Swiggy">Swiggy</option>
                    <option value="Dineout">Dineout</option>
                    <option value="Takeaway">Takeaway</option>
                  </FormSelect>
                </div>
                <div className="col-sm-4">
                  <FormGroup>
                    <label>Customer Name (*)</label>
                    <FormInput type="text" />
                  </FormGroup>
                </div>
                <div className="col-sm-4">
                  <FormGroup>
                    <label>Customer Phone Number</label>
                    <FormInput type="number" />
                  </FormGroup>
                </div>
              </div>
              <label>Order</label>
              <div className="row add-custom-order">
                <div className="col-sm-6">
                  <OrderCustomer />
                </div>
                <div className="col-sm-6  border rounded">
                  <FoodCategory />
                </div>
              </div>
              <div className="footer mt-3">
                <div className="row">
                  <div className="col-sm-4">
                    <label className="footer-heading">
                      Select Payment Mode
                    </label>
                  </div>
                  <div className="col-sm-4">
                    <label className="footer-heading">Payment Remark</label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-4">
                    <FormSelect>
                      <option value="">Select payment method ...</option>
                      <option value="Cash">Cash</option>
                      <option value="Paytm">Paytm</option>
                      <option value="PhonePe">PhonePe</option>
                      <option value="Debit Card">Debit Card</option>
                      <option value="Credit Card">Credit Card</option>
                      <option value="Google Pay">Google Pay</option>
                      <option value="Other">Other</option>
                    </FormSelect>
                  </div>
                  <div className="col-sm-4">
                    <FormInput placeholder="My form input" />;
                  </div>
                  <div className="col-sm-2">
                    <button className="btn btn-success w-75">Print Bill</button>
                  </div>
                  <div className="col-sm-2">
                    <button className="btn btn-primary w-75">Confirm</button>
                  </div>
                </div>
              </div>
            </DropdownMenu>
          </Dropdown>
        </Row>
      </Container>
    );
  }
}

export default AddCustomer;
