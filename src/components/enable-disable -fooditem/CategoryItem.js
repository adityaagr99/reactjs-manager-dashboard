import React, { Component } from "react";
import { toggleFoodItem } from "../../redux/actions/category.action";
import { connect } from "react-redux";
export class CategoryItem extends Component {
  onchange = () => {};

  toggle = (ischecked, caffeID, foodID) => {
    let data = {
      caffeID: caffeID,
      foodID: foodID
    };
    let type;
    if (ischecked) {
      type = "disable";
    } else {
      type = "enable";
    }
    this.props.toggleFoodItem(type, data);
  };

  componentDidMount() {
    this.setState({ isChecked: this.props.categoryItemData.isavail });
  }

  render() {
    const { categoryItemData, auth, category } = this.props;
    return (
      <div className="p-0 pb-3 card-body">
        <table className="table mb-0">
          <tbody>
            <tr>
              <td className="w-50">{categoryItemData.foodname}</td>
              <td className="w-50">
                <label className="custom-control custom-toggle custom-toggle-sm">
                  <input
                    id=""
                    type="checkbox"
                    className="custom-control-input"
                    checked={categoryItemData.isavail ? true : false}
                    onChange={this.onchange}
                  />
                  <label
                    id=""
                    className="custom-control-label"
                    aria-hidden="true"
                    onClick={() => {
                      if (!category.loading) {
                        this.toggle(
                          categoryItemData.isavail,
                          auth.caffeID,
                          categoryItemData.foodid
                        );
                      }
                    }}
                  ></label>
                  <span
                    className="custom-control-description"
                    onClick={() => {
                      if (!category.loading) {
                        this.toggle(
                          categoryItemData.isavail,
                          auth.caffeID,
                          categoryItemData.foodid
                        );
                      }
                    }}
                  >
                    {categoryItemData.isavail ? "Disable" : "Enable"}
                  </span>
                </label>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  category: state.category
});

export default connect(mapStateToProps, { toggleFoodItem })(CategoryItem);
