import React, { Component } from "react";
import { toggleAddonItem } from "../../redux/actions/category.action";
import { connect } from "react-redux";
export class AddonItem extends Component {
  onchange = () => {};
  toggle = (ischecked, caffeID, addonid) => {
    let data = {
      caffeID: caffeID,
      addonid: addonid
    };
    let type;
    if (ischecked) {
      type = "disable";
    } else {
      type = "enable";
    }
    this.props.toggleAddonItem(type, data);
  };
  render() {
    const { addon, category, auth } = this.props;
    return (
      <div className="p-0 pb-3 card-body">
        <table className="table mb-0">
          <tbody>
            <tr>
              <td className="w-50">{addon.addonname}</td>
              <td className="w-50">
                <label className="custom-control custom-toggle custom-toggle-sm">
                  <input
                    id=""
                    type="checkbox"
                    className="custom-control-input"
                    checked={addon.is_avail ? true : false}
                    onChange={this.onchange}
                  />
                  <label
                    id=""
                    className="custom-control-label"
                    aria-hidden="true"
                    onClick={() => {
                      if (!category.loading) {
                        this.toggle(
                          addon.is_avail,
                          auth.caffeID,
                          addon.addonid
                        );
                      }
                    }}
                  ></label>
                  <span
                    className="custom-control-description"
                    onClick={() => {
                      if (!category.loading) {
                        this.toggle(
                          addon.is_avail,
                          auth.caffeID,
                          addon.addonid
                        );
                      }
                    }}
                  >
                    {addon.is_avail ? "Disable" : "Enable"}
                  </span>
                </label>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

const maStateToProps = state => ({
  auth: state.auth,
  category: state.category,
  addonlist: state.category.addonlist
});

export default connect(maStateToProps, { toggleAddonItem })(AddonItem);
