import React, { Component } from "react";
import { Card, CardHeader, Collapse, CardBody } from "shards-react";
import CategoryItem from "./CategoryItem";

export class Category extends Component {
  state = {
    collapse: false
  };

  toggle = () => {
    this.setState({
      collapse: !this.state.collapse
    });
  };

  renderCategoryItem = data => {
    if (!data) return <div> No data </div>;
    if (data)
      return data.map((item, i) => (
        <CategoryItem categoryItemData={item} key={i} />
      ));
  };

  render() {
    const { collapse } = this.state;
    const { categoryItem } = this.props;

    return (
      <Card className="mb-4 overflow-hidden w-100 ">
        <CardHeader
          onClick={this.toggle}
          className="d-flex justify-content-between align-items-center"
        >
          <div> {categoryItem.foodcategory.toUpperCase()} </div>
          <div>
            {collapse ? (
              <i
                className="material-icons"
                style={{
                  fontSize: "25px"
                }}
              >
                keyboard_arrow_up
              </i>
            ) : (
              <i
                className="material-icons"
                style={{
                  fontSize: "25px"
                }}
              >
                keyboard_arrow_down
              </i>
            )}
          </div>
        </CardHeader>
        <Collapse open={this.state.collapse}>
          <CardBody> {this.renderCategoryItem(categoryItem.data)} </CardBody>
        </Collapse>
      </Card>
    );
  }
}

export default Category;
