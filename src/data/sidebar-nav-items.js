export default function () {
  return [{
      title: "Order Manager",
      htmlBefore: '<i class="material-icons">receipt</i>',
      to: "/order-manager",
    },
    {
      title: "Enable Disable Food Item",
      htmlBefore: '<i class="material-icons">swap_horiz</i>',
      to: "/enable-food-item",
    },
    {
      title: "Add Customer",
      htmlBefore: '<i class="material-icons">add</i>',
      to: "/add-customer",
    },
  ];
}
